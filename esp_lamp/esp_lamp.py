import paho.mqtt.client as mqtt_client 
import random 
import time 
 
def on_connect(client, userdata, flags, rc): 
    if rc == 0: 
        print("Connected to MQTT Broker!") 
    else: 
        print("Failed to connect, return code %d\n", rc) 
 

def start_loop(topic):
    start_time, end_time = 20 , 40
    min_end_time = 30
    one_period = 60
    seconds=1
    current_end=end_time
    cmd = '1'
    while True:
        if seconds==1:
            print(f'Led {start_time}-{current_end} second')
        
        print(f'{seconds} send command is {cmd}')

        seconds+=1
        if seconds>=start_time and seconds<=current_end:
            cmd = '0'
        else:
            cmd = '1'

        if seconds>one_period:
            seconds=1
            current_end-=1

        if current_end<min_end_time:
            current_end=end_time
        
        client.publish(f"{topic}/command", cmd)

        time.sleep(1)

topic = "topic_1"
broker = "broker.emqx.io"  

client = mqtt_client.Client(f'lab_{random.randint(10000, 99999)}') 
client.on_connect = on_connect 
client.connect(broker)

start_loop(topic)